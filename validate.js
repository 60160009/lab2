module.exports = {
    isUserNameValid: function(username){
        if(username.length<3 || username.length>15 ){
            return false;
        }

        if (username.toLowerCase() !== username) {
            return false;
        }
        return true;
    },
    isAgeValid: function (age) {
        if(isNaN(age) ){
            return false ;
        }
        else if(age < 18 || age > 100){
            return false ;
        }
        return true ;
    },
    isPasswordValid  : function (password){
        if(password.length < 8){
            return false ;
        }
        if(password.toLowerCase() == password){
            return false ;
        }
        if( !(new RegExp("^(?=.*[0-9]{3})").test(password)) ){
            return false ;
        }
        if( !(new RegExp("^(?=.*[!@#\$%\^&\*]{1})").test(password)) ){
            return false ;
        }
        return true ;
    },
    isDateValid : function(day,month,year){
        if(day < 1 || day > 31){
            return false;
        }
        if(month < 1 || month > 12 ){
            return false ;
        }
        if(year < 1970  || year > 2020){
            return false;
        }
        if(month == 4 || month == 6 || month == 9 || month == 11){
            if((day<31)) return false ;
        }
        if(month == 2){
            if ((year % 100 == 0) && (year % 400 == 0)){
                if((day>29)) return false ;
            }else{
                console.log(day);
                if((day>28)) return false ;
            }
        }
        return true ;
    }

}